# Spring Demo

This is a spring boot demo project to show up. There are several spring boot modules integrated into this project such as:
1. Spring Security  (Basic Authorization)
2. Spring Data (JPA Hibernate)
3. Log4j2 (logging)
3. Spring JMS (Active MQ)