/**
 * @author Ruslan
 */
package uz.ruslan.spring.productType;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductTypeRepo extends JpaRepository<ProductType, Long> {
}
