/**
 * @author Ruslan
 */

package uz.ruslan.spring.productType;

import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.springframework.http.ResponseEntity.ok;

@Service
@AllArgsConstructor
public class ProductTypeService {
    private final ProductTypeRepo productTypeRepo;

    @PostConstruct
    public void init() {
        if (productTypeRepo.findAll().isEmpty()) {
            List<ProductType> productTypes = new ArrayList<>();
            productTypes.add(new ProductType("Phone"));
            productTypes.add(new ProductType("Notebook"));
            productTypes.add(new ProductType("PC"));
            productTypes.add(new ProductType("Mouse"));
            productTypeRepo.saveAll(productTypes);
        }
    }

    @Cacheable(cacheNames = "productType")
    public ResponseEntity<List<ProductType>> getAllProductTypes() {
        return ok(productTypeRepo.findAll());
    }

    public Optional<ProductType> getById(Long typeId) {
        return productTypeRepo.findById(typeId);
    }

}
