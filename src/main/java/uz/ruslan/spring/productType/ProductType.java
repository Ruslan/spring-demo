/**
 * @author Ruslan
 */

package uz.ruslan.spring.productType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.ruslan.spring.product.Product;
import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "product_type")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductType {
    @Transient
    private static final String seqName = "product_type_id_seq";
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = seqName)
    @SequenceGenerator(name = seqName, sequenceName = seqName, allocationSize = 1)
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.EAGER)
    @JoinColumn(name = "product_type_id")
    private List<Product> productList;

    public ProductType(String name) {
        this.name = name;
    }
}
