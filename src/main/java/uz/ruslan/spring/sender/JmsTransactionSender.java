/**
 * @author Ruslan
 */

package uz.ruslan.spring.sender;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import uz.ruslan.spring.transaction.Transaction;

@Service
public class JmsTransactionSender implements TransactionSender {
    private final JmsTemplate jmsTemplate;

    @Autowired
    public JmsTransactionSender(@Qualifier("jmsTopic") JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    @Override
    public void sendTransaction(Transaction transaction) {
        jmsTemplate.convertAndSend("transaction", transaction);
    }
}
