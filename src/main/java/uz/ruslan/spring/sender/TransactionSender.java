/**
 * @author Ruslan
 */

package uz.ruslan.spring.sender;

import uz.ruslan.spring.transaction.Transaction;

public interface TransactionSender {
    void sendTransaction(Transaction transaction);
}
