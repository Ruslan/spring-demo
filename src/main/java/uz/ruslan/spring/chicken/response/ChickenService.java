/**
 * @author Ruslan
 */

package uz.ruslan.spring.chicken.response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import uz.ruslan.spring.common.Message;
import uz.ruslan.spring.product.ProductDto;

@Service
public class ChickenService {
    private final RestTemplate restTemplate;
    private static String ADMIN_TOKEN = "dc764366-68fa-48bc-bf04-c96d975679ec";
    private static String GET_CUSTOMER_URL = "http://localhost:8082/api/customer";
    private Logger logger = LogManager.getLogger(ChickenService.class);

    public ChickenService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public ResponseEntity<?> getChickenCustomers(){
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", ADMIN_TOKEN);
        try {
            logger.info("GET CUSTOMER: -> REQUEST");
            ProductDto request = new ProductDto();
            request.setName("asdf");
            request.setType("dfs");
            ResponseEntity<CustomerResponse> customerResponse = restTemplate.exchange(GET_CUSTOMER_URL, HttpMethod.GET, new HttpEntity<>(request, headers), CustomerResponse.class);
            logger.info("GET CUSTOMER: -> RESPONSE\"" +
                    "RESPONSE: " + customerResponse.getBody());
            return customerResponse;
        } catch (ResourceAccessException ex) {
            logger.error("TIMOUT\n" +
                    "URL: " + GET_CUSTOMER_URL +
                    "MESSAGE: " + ex.getMessage());
        } catch (HttpClientErrorException | HttpServerErrorException ex) {
            logger.error("CLIENT ERROR\n" +
                    "URL: " + GET_CUSTOMER_URL + "\n" +
                    "MESSAGE: " + ex.getResponseBodyAsString());
        }
        return Message.notFound("Chicken server error");
    }
}
