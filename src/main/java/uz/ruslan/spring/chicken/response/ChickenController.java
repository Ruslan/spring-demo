/**
 * @author Ruslan
 */

package uz.ruslan.spring.chicken.response;

import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping(value = "/api/chicken", produces = MediaType.APPLICATION_JSON_VALUE)
public class ChickenController {
    private final ChickenService chickenService;

    @GetMapping("/customers")
    public ResponseEntity<?> getChickenCustomers(){
        return chickenService.getChickenCustomers();
    }
}
