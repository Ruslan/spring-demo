package uz.ruslan.spring.chicken.response;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class CustomerResponse{

	@JsonProperty("data")
	private List<DataItem> data;

	@JsonProperty("errorMessage")
	private String errorMessage;

	@JsonProperty("timestamp")
	private long timestamp;

	public void setData(List<DataItem> data){
		this.data = data;
	}

	public List<DataItem> getData(){
		return data;
	}

	public void setErrorMessage(String errorMessage){
		this.errorMessage = errorMessage;
	}

	public String getErrorMessage(){
		return errorMessage;
	}

	public void setTimestamp(long timestamp){
		this.timestamp = timestamp;
	}

	public long getTimestamp(){
		return timestamp;
	}

	@Override
 	public String toString(){
		return 
			"CustomerResponse{" + 
			"data = '" + data + '\'' + 
			",errorMessage = '" + errorMessage + '\'' + 
			",timestamp = '" + timestamp + '\'' + 
			"}";
		}
}