package uz.ruslan.spring.chicken.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class DataItem{

	@JsonProperty("phoneNumber")
	private String phoneNumber;

	@JsonProperty("address")
	private String address;

	@JsonProperty("balance")
	private double balance;

	@JsonProperty("familyName")
	private String familyName;

	@JsonProperty("imageUrl")
	private String imageUrl;

	@JsonProperty("totalDebt")
	private double totalDebt;

	@JsonProperty("name")
	private String name;

	@JsonProperty("company")
	private String company;

	@JsonProperty("id")
	private int id;

	@JsonProperty("managerName")
	private String managerName;

	@JsonProperty("email")
	private String email;

	public void setPhoneNumber(String phoneNumber){
		this.phoneNumber = phoneNumber;
	}

	public String getPhoneNumber(){
		return phoneNumber;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setBalance(double balance){
		this.balance = balance;
	}

	public double getBalance(){
		return balance;
	}

	public void setFamilyName(String familyName){
		this.familyName = familyName;
	}

	public String getFamilyName(){
		return familyName;
	}

	public void setImageUrl(String imageUrl){
		this.imageUrl = imageUrl;
	}

	public String getImageUrl(){
		return imageUrl;
	}

	public void setTotalDebt(double totalDebt){
		this.totalDebt = totalDebt;
	}

	public double getTotalDebt(){
		return totalDebt;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setCompany(String company){
		this.company = company;
	}

	public String getCompany(){
		return company;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setManagerName(String managerName){
		this.managerName = managerName;
	}

	public String getManagerName(){
		return managerName;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"phoneNumber = '" + phoneNumber + '\'' + 
			",address = '" + address + '\'' + 
			",balance = '" + balance + '\'' + 
			",familyName = '" + familyName + '\'' + 
			",imageUrl = '" + imageUrl + '\'' + 
			",totalDebt = '" + totalDebt + '\'' + 
			",name = '" + name + '\'' + 
			",company = '" + company + '\'' + 
			",id = '" + id + '\'' + 
			",managerName = '" + managerName + '\'' + 
			",email = '" + email + '\'' + 
			"}";
		}
}