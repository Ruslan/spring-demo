/**
 * @author Ruslan
 */

package uz.ruslan.spring.order;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class Order {
    private Long id;
    private Long userId;
    private Long productId;
    private float weight;
    private float totalPrice;

}
