/**
 * @author Ruslan
 */

package uz.ruslan.spring.order;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.ruslan.spring.common.Message;

@Service
@AllArgsConstructor
public class OrderService {
    private final KafkaOrderMessagingService messagingService;

    public ResponseEntity<Message> makeOrder(Order order) {
//        messagingService.sendOrder(order);
        return Message.success();
    }
}
