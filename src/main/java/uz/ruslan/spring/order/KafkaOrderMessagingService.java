///**
// * @author Ruslan
// */
//
package uz.ruslan.spring.order;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

//
//import lombok.RequiredArgsConstructor;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.kafka.annotation.KafkaListener;
//import org.springframework.kafka.core.KafkaTemplate;
//import org.springframework.stereotype.Service;
//
@Service
@RequiredArgsConstructor
public class KafkaOrderMessagingService {
//    private static Logger logger = LogManager.getLogger(KafkaOrderMessagingService.class);
//    private final KafkaTemplate<String, String> kafkaTemplate;
//    @Value("${kafka.topic.boot}")
//    private String topicBoot;
//
//    @Override
//    public void sendOrder(Order order) {
//        logger.info("Sending message {} to default topic", order);
//        kafkaTemplate.send(topicBoot, order.toString());
//    }
//
//    @KafkaListener(topics = "${kafka.topic.boot}")
//    public void receiveOrder(String order) {
//        logger.info("Received message {} from default topic", order);
//    }
}
