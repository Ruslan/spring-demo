/**
 * @author Ruslan
 */

package uz.ruslan.spring.order;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.ruslan.spring.common.Message;

@RestController
@RequestMapping(value = "/api/order")
@AllArgsConstructor
public class OrderController {

    private final OrderService orderService;

    @PostMapping
    public ResponseEntity<Message> makeOrder(@RequestBody Order order) {
        return orderService.makeOrder(order);
    }
}
