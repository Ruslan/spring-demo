/**
 * @author Ruslan
 */
package uz.ruslan.spring.order;

public interface OrderMessagingService {
    public void sendOrder(Order order);
}
