package uz.ruslan.spring.transaction;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.annotation.Generated;
import java.io.Serializable;

@Generated("com.robohorse.robopojogenerator")
public class Transaction implements Serializable {

	@JsonProperty("amount")
	private int amount;

	@JsonProperty("debitAccount")
	private String debitAccount;

	@JsonProperty("creditAccount")
	private String creditAccount;

	public void setAmount(int amount){
		this.amount = amount;
	}

	public int getAmount(){
		return amount;
	}

	public void setDebitAccount(String debitAccount){
		this.debitAccount = debitAccount;
	}

	public String getDebitAccount(){
		return debitAccount;
	}

	public void setCreditAccount(String creditAccount){
		this.creditAccount = creditAccount;
	}

	public String getCreditAccount(){
		return creditAccount;
	}

	@Override
 	public String toString(){
		return 
			"Transaction{" + 
			"amount = '" + amount + '\'' + 
			",debitAccount = '" + debitAccount + '\'' + 
			",creditAccount = '" + creditAccount + '\'' + 
			"}";
		}
}