/**
 * @author Ruslan
 */

package uz.ruslan.spring.transaction;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.ruslan.spring.common.Message;
import uz.ruslan.spring.sender.JmsTransactionSender;


@RestController
@RequestMapping(value = "/api/transaction")
public class TransactionController {
    private final JmsTransactionSender jmsTransactionSender;

    public TransactionController(JmsTransactionSender jmsTransactionSender) {
        this.jmsTransactionSender = jmsTransactionSender;
    }

    @PostMapping
    public ResponseEntity<Message> sendTransaction(@RequestBody Transaction transaction) {
        jmsTransactionSender.sendTransaction(transaction);
        return Message.success();
    }
}
