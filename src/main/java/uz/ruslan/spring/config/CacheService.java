/**
 * @author Ruslan
 */

package uz.ruslan.spring.config;

import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import uz.ruslan.spring.cache.CacheNames;

import java.util.Optional;

@Configuration
@EnableCaching
@EnableScheduling
public class CacheService {
    private final CacheManager cacheManager;

    public CacheService(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }

    public void clearAllCache(){
        cacheManager.getCacheNames().forEach(this::clearCacheByName);
    }

    public void clearCacheByName(String cacheName){
        Optional.ofNullable(cacheManager.getCache(cacheName))
                .ifPresent(Cache::clear);

    }

    @Scheduled(cron = "0 0 * * * ?")
    public void clearProductByTypeCache(){
        clearCacheByName(CacheNames.PRODUCT_TYPE_CACHE);
    }
}
