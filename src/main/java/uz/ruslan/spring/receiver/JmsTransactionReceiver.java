/**
 * @author Ruslan
 */

package uz.ruslan.spring.receiver;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import uz.ruslan.spring.transaction.Transaction;

import javax.jms.JMSException;

@Component
public class JmsTransactionReceiver implements TransactionReceiver {
    private Logger logger = LogManager.getLogger(JmsTransactionReceiver.class);

    @JmsListener(destination = "transaction", containerFactory = "topicListener")
    @Override
    public void receive(ActiveMQTextMessage activeMQMessage) throws JMSException {
//        Message message = jmsTemplate.receive("transaction");
//        if (Objects.isNull(message)) {
//            logger.error("Message is null");
//            return;
//        }
//        Transaction transaction = (Transaction) messageConverter.fromMessage(message);
        logger.info(activeMQMessage.getText());
        try {
            Transaction transaction = new ObjectMapper().readValue(activeMQMessage.getText(), Transaction.class);
            logger.info("Message is received {} ", transaction.toString());
        } catch (JsonProcessingException e) {
            logger.error("Message is received from {} destination. Transaction can not be parsed", "transaction");
        }
    }
}
