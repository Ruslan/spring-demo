/**
 * @author Ruslan
 */
package uz.ruslan.spring.receiver;

import org.apache.activemq.command.ActiveMQMessage;
import org.apache.activemq.command.ActiveMQTextMessage;
import uz.ruslan.spring.transaction.Transaction;

import javax.jms.JMSException;

public interface TransactionReceiver {
    void receive(ActiveMQTextMessage message) throws JMSException;
}
