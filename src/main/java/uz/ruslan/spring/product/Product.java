/**
 * @author Ruslan
 */

package uz.ruslan.spring.product;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import uz.ruslan.spring.productType.ProductType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
@Table(name = "product")
@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(name = "name", nullable = false)
    @NotNull
    @Size(min = 2, message = "Name can not be less then 2 symbols")
    private String name;
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "product_type_id")
    private ProductType productType;

    public Product(@NotNull @Size(min = 2, message = "Name can not be less then 2 symbols") String name) {
        this.name = name;
    }
}

