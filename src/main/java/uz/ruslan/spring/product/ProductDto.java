/**
 * @author Ruslan
 */

package uz.ruslan.spring.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import uz.ruslan.spring.productType.ProductType;

@Data
@RequiredArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductDto {
    @JsonProperty(value = "id")
    private Long id;
    @JsonProperty(value = "name")
    private String name;
    @JsonProperty(value = "typeName")
    private String type;
    @JsonProperty(value = "typeId")
    private Long typeId;

    public ProductDto(Long id, String name, String type) {
        this.id = id;
        this.name = name;
        this.type = type;
    }

    public static Product fromDto(ProductDto productDto, ProductType productType) {
        return new Product(productDto.getId(), productDto.getName(), productType);
    }

    public static ProductDto toDto(Product product) {
        return new ProductDto(product.getId(), product.getName(), product.getProductType().getName());
    }
}
