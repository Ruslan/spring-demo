/**
 * @author Ruslan
 */

package uz.ruslan.spring.product;

import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.ruslan.spring.cache.CacheNames;
import uz.ruslan.spring.common.Message;
import uz.ruslan.spring.productType.ProductType;
import uz.ruslan.spring.productType.ProductTypeService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.http.ResponseEntity.badRequest;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.util.StringUtils.isEmpty;
import static uz.ruslan.spring.common.Message.notFound;

@Service
@AllArgsConstructor
public class ProductService {
    private final ProductRepo productRepo;
    private final ProductTypeService productTypeService;


    public ResponseEntity<List<ProductDto>> getProducts() {
        return ok(productRepo.findAll()
                .stream()
                .map(ProductDto::toDto)
                .collect(Collectors.toList()));
    }

    @CachePut(cacheNames = CacheNames.PRODUCT_TYPE_CACHE, key = "#productDto.typeId")
    public ResponseEntity<Message> addProduct(ProductDto productDto) {
        if (isEmpty(productDto.getName())) {
            return badRequest().body(new Message("Mahsulot nomini kiriting!"));
        }
        Optional<ProductType> productTypeOpt = productTypeService.getById(productDto.getTypeId());
        if (productTypeOpt.isEmpty()) {
            return notFound("Product type not found");
        }
        ProductType productType = productTypeOpt.get();
        productRepo.save(ProductDto.fromDto(productDto, productType));
        return Message.success();
    }

    public ResponseEntity<Message> deleteProduct(Long id) {
        productRepo.deleteById(id);
        return Message.success();
    }

    @Cacheable(cacheNames = CacheNames.PRODUCT_TYPE_CACHE, key = "#typeId", unless = "#typeId == null")
    public ResponseEntity<List<ProductDto>> getProductsByType(Long typeId) {
        return ResponseEntity.ok(productRepo.findByProductTypeIdOrderByName(typeId)
                .stream()
                .map(ProductDto::toDto)
                .collect(Collectors.toList()));
    }
}
