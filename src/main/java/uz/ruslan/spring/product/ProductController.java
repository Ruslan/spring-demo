/**
 * @author Ruslan
 */

package uz.ruslan.spring.product;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import uz.ruslan.spring.common.Message;
import uz.ruslan.spring.user.User;

import java.util.List;

@RestController
@RequestMapping(value = "/api/product", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public ResponseEntity<List<ProductDto>> getAllProducts(@AuthenticationPrincipal User user) {
        return productService.getProducts();
    }

    @GetMapping("/byType")
    public ResponseEntity<List<ProductDto>> getProductsByType(@RequestParam("typeId") Long typeId) {
        return productService.getProductsByType(typeId);
    }

    @PostMapping
    public ResponseEntity<Message> addNewProduct(@RequestBody ProductDto productDto) {
        return productService.addProduct(productDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Message> deleteProduct(@PathVariable("id") Long id){
        return productService.deleteProduct(id);
    }
}
