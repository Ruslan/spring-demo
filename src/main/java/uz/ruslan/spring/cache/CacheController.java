/**
 * @author Ruslan
 */

package uz.ruslan.spring.cache;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.ruslan.spring.common.Message;
import uz.ruslan.spring.config.CacheService;

@RestController
@RequestMapping(value = "/api/cache")
public class CacheController {

    private final CacheService cacheService;

    public CacheController(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    @PostMapping("/clear")
    public ResponseEntity<Message> clearAllCache() {
        cacheService.clearAllCache();
        return Message.success();
    }

    @PostMapping("/clear/{name}")
    public ResponseEntity<Message> clearCacheByName(@PathVariable("name") String cacheName) {
        cacheService.clearCacheByName(cacheName);
        return Message.success();
    }

}
