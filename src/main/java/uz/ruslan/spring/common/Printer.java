/**
 * @author Ruslan
 */
package uz.ruslan.spring.common;

public interface Printer {
    void print();
}
