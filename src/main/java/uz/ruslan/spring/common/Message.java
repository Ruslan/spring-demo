/**
 * @author Ruslan
 */

package uz.ruslan.spring.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@Data
@AllArgsConstructor
public class Message {
    private String message;

    public static ResponseEntity<Message> success(){
        return ResponseEntity.ok(new Message("success"));
    }

    public static ResponseEntity<Message> notFound(String message) {
        return new ResponseEntity<>(new Message(message), HttpStatus.NOT_FOUND);
    }
}
