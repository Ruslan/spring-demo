/**
 * @author Ruslan
 */

package uz.ruslan.spring.common;

public class Canon implements Printer {
    @Override
    public void print() {
        System.out.println("Canon printer");
    }
}
