/**
 * @author Ruslan
 */

package uz.ruslan.spring.user;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import uz.ruslan.spring.common.Message;

import java.util.Optional;

@Service
public class UserService implements UserDetailsService {

    private final UserRepo userRepo;
    private final BCryptPasswordEncoder encoder;
    private Logger logger = LogManager.getLogger(UserService.class);

    public UserService(UserRepo userRepo, BCryptPasswordEncoder encoder) {
        this.userRepo = userRepo;
        this.encoder = encoder;
    }


    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        Optional<User> userOptional = userRepo.findByUserName(userName);
        if (userOptional.isPresent()) {
            return userOptional.get();
        }
        throw new UsernameNotFoundException("User '" + userName + "' not found");
    }

    public ResponseEntity<Message> registerUser(UserDto userDto) {
        logger.info(userDto.toString());
        User user = new User(userDto.getUserName(), encoder.encode(userDto.getPassword()), Role.USER);
        userRepo.save(user);
        return Message.success();
    }
}
