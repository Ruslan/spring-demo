/**
 * @author Ruslan
 */

package uz.ruslan.spring.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    @JsonProperty("userName")
    @NotNull
    @Size(min = 1, message = "Username can not be less than 1 symbols")
    private String userName;
    @JsonProperty("password")
    @NotNull
    @Size(min=3, message = "Password must be at least 3 symbols")
    private String password;
}
