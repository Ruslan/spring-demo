/**
 * @author Ruslan
 */

package uz.ruslan.spring.user;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import uz.ruslan.spring.common.Message;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/user")
@AllArgsConstructor
public class UserController {
    private final UserService userService;

    @PostMapping("/register")
    public ResponseEntity<Message> register(@Valid @RequestBody UserDto userDto) {
        return userService.registerUser(userDto);
    }

}
