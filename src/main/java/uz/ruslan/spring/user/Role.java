/**
 * @author Ruslan
 */
package uz.ruslan.spring.user;

public enum Role {
    USER, MANAGER, ADMIN
}
